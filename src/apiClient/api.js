import axios from "axios";

const requestApi = (config, onSuccess, onFailure) => {
  axios
    .request(config)
    .then((response) => onSuccess(response))
    .catch((error) => onFailure(error));
};

export default requestApi;
