import requestApi from "./api";

const SCHEME = "https://";
const BASE_URL = "ashwin-issue-tracker-backend.herokuapp.com/api";

const GET_ALL_PROJECTS = "/projects";
const GET_ALL_ISSUES = "/issues";
const GET_ALL_COMMENTS = "/comments";

export const getAllProjects = (onSuccess, onFailure) => {
  let config = {
    method: "GET",
    baseURL: SCHEME + BASE_URL,
    url: GET_ALL_PROJECTS,
    headers: { "Content-Type": "application/json" },
  };
  return requestApi(config, onSuccess, onFailure);
};

export const postProject = (data, onSuccess, onFailure) => {
  let config = {
    method: "POST",
    baseURL: SCHEME + BASE_URL,
    url: GET_ALL_PROJECTS,
    headers: { "Content-Type": "application/json" },
    data: JSON.stringify(data),
  };
  return requestApi(config, onSuccess, onFailure);
};

export const deleteSelectedProject = (projectId, onSuccess, onFailure) => {
  let config = {
    method: "DELETE",
    baseURL: SCHEME + BASE_URL,
    url: GET_ALL_PROJECTS + "/" + projectId.toString(),
    headers: { "Content-Type": "application/json" },
  };
  return requestApi(config, onSuccess, onFailure);
};

export const createNewIssue = (data, onSuccess, onFailure) => {
  let config = {
    method: "POST",
    baseURL: SCHEME + BASE_URL,
    url: GET_ALL_ISSUES,
    headers: { "Content-Type": "application/json" },
    data: JSON.stringify(data),
  };
  return requestApi(config, onSuccess, onFailure);
};

export const getAllIssueWithProjectId = (projectId, onSuccess, onFailure) => {
  let config = {
    method: "GET",
    baseURL: SCHEME + BASE_URL,
    url: GET_ALL_ISSUES + "/" + projectId.toString(),
    headers: { "Content-Type": "application/json" },
  };
  return requestApi(config, onSuccess, onFailure);
};

export const deleteIssue = (issueId, onSuccess, onFailure) => {
  let config = {
    method: "DELETE",
    baseURL: SCHEME + BASE_URL,
    url: GET_ALL_ISSUES + "/" + issueId.toString(),
    headers: { "Content-Type": "application/json" },
  };
  return requestApi(config, onSuccess, onFailure);
};

export const updateIssue = (issueId, data, onSuccess, onFailure) => {
  let config = {
    method: "PATCH",
    baseURL: SCHEME + BASE_URL,
    url: GET_ALL_ISSUES + "/" + issueId.toString(),
    headers: { "Content-Type": "application/json" },
    data: JSON.stringify(data),
  };
  return requestApi(config, onSuccess, onFailure);
};

export const addComments = (data, onSuccess, onFailure) => {
  let config = {
    method: "POST",
    baseURL: SCHEME + BASE_URL,
    url: GET_ALL_COMMENTS,
    headers: { "Content-Type": "application/json" },
    data: JSON.stringify(data),
  };
  return requestApi(config, onSuccess, onFailure);
};

export const retrieveComments = (issueId, onSuccess, onFailure) => {
  let config = {
    method: "GET",
    baseURL: SCHEME + BASE_URL,
    url: GET_ALL_COMMENTS + "/" + issueId.toString(),
    headers: { "Content-Type": "application/json" },
  };
  return requestApi(config, onSuccess, onFailure);
};

export const updateComments = (commentId, data, onSuccess, onFailure) => {
  let config = {
    method: "PATCH",
    baseURL: SCHEME + BASE_URL,
    url: GET_ALL_COMMENTS + "/" + commentId.toString(),
    headers: { "Content-Type": "application/json" },
    data: JSON.stringify(data),
  };
  return requestApi(config, onSuccess, onFailure);
};

export const deleteComments = (commentId, onSuccess, onFailure) => {
  let config = {
    method: "DELETE",
    baseURL: SCHEME + BASE_URL,
    url: GET_ALL_COMMENTS + "/" + commentId.toString(),
    headers: { "Content-Type": "application/json" },
  };
  return requestApi(config, onSuccess, onFailure);
};
