import DeleteIcon from '@material-ui/icons/Delete';

const IssueCard = (props) => {
    return (
        <>
        <div className="issue-card">
            <div className="delete-btn-container">
                <DeleteIcon onClick={props.onDeleteClick}/>
            </div>
            <div onClick= {props.onIssueCardClick} className="issue-card-container">
                <h4>ISSUE TITLE</h4>
                <p>{props.issueTitle}</p>
                <h4>ISSUE CONTENT</h4>
                <p id="issue-card-content">{props.issueContent}</p>
            </div>
        </div>
        </>
    );
}
 
export default IssueCard;