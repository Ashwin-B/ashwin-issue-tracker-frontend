import "./components.css";
import DeleteIcon from '@material-ui/icons/Delete';

const Card = (props) => {

    return ( 
        
        <div className="card-container">
            <div className="card-btn-container">
                <DeleteIcon id="delete-btn" onClick = {props.onClick}/>
            </div>
            <div onClick={props.onClickRedirect} className="card-content-container">
                <p id="card-title">PROJECT NAME</p>
                <p>{props.projectName}</p>
            </div>
        </div>
     );
}
 
export default Card;