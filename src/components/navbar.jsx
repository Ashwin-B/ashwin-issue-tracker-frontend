import Button from "./button"
import InputField from "./inputField"

const buttonStyle = {
    backgroundColor : "white",
    color : "#663a82",
    fontSize : "1em",
    height : "2em",
    marginRight : "1em",

}

const Navbar = (props) => {
    return (
        <div className="navbar-container">
            <p id="company-title">ISSUE TRACKER</p>
            <div className="create-project-container">
                <InputField style={props.inputStyle} name={props.name} placeholder={props.placeholder} onChange={props.onInputChange} value={props.updatedValue}/>
                <Button onClick={props.onCreateProjectClick} id="btn-create-project" style={buttonStyle} name={props.buttonName}/>
            </div>
        </div>
    );
}
 
export default Navbar;
 
