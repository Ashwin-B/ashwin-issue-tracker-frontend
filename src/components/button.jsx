import "./components.css";

const Button = (props) => {
    return (
        <button style={props.style} className="btn" onClick={props.onClick}>{props.name}</button>
    );
}

 
export default Button;