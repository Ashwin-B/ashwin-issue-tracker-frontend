import "./components.css";

const InputField = (props) => {
    return ( 
        <input className="input-field" style={props.style} name={props.name} type="text" placeholder={props.placeholder} value={props.value} onChange={props.onChange}/>
     );
}
 
export default InputField;