import DeleteIcon from '@material-ui/icons/Delete';

const Comments = (props) => {
    return ( 
        <div className="comments-added">
            <p id="comments-content">{props.comment}</p>
            <DeleteIcon onClick = {props.onDeleteClick} className="delete-icon"/>
        </div>
     );
}
 
export default Comments;