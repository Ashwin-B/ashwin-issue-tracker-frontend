import { Route, Switch } from "react-router-dom";
import LandingScreen from "./screens/landingScreen";
import ProjectDashboard from "./screens/projectScreen";
import IssueScreen from "./screens/issueScreen";
import ErrorScreen from "./screens/errorScreen";
import IssueDetailScreen from "./screens/issuesDetailScreen";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/project/:name/issues/:id" component={IssueDetailScreen} />
        <Route path="/projects/:name" component={IssueScreen} />
        <Route path="/projects" component={ProjectDashboard} />
        <Route path="/error" component={ErrorScreen} />
        <Route path="/" component={LandingScreen} />
      </Switch>
    </div>
  );
}

export default App;
