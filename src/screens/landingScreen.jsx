import "./screens.css";
import {Link} from "react-router-dom"
import Button from "../components/button";

const LandingScreen = (props) => {
    return ( 
        <div className="outer-container">
            <div className="content-container">
                <div className="title">
                    <p id="lp-title">ISSUE TRACKER</p>
                    <p id="lp-subtitle">A simple way to track your project issues</p>
                 </div>
                 <Link to="/projects">
                    <div className="btn-container">
                        <Button name="Get Started"/>
                    </div>
                 </Link>
            </div>
        </div>
     );
}
 
export default LandingScreen;