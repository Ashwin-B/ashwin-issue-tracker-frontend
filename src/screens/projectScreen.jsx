import React, { Component } from 'react';
import Joi from "joi";
import Navbar from "../components/navbar";
import Card from "../components/cards"
import { getAllProjects, postProject, deleteSelectedProject } from '../apiClient/apiRequest';


const projectNameSchema = Joi.object({
    project : Joi.string().min(3).max(20).required().label("Project Name")
})

class ProjectDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projects : [],
            newProjectInput : "",
            projectInputError : "",
            error : false,
            errorMessage : "",
        }
        
    }

    componentDidMount() {
        getAllProjects(this.onGetAllProjectSuccess, this.onGetAllProjectFailure)
    }

    onGetAllProjectSuccess = (res) => {
        this.setState({projects : res.data})
    }

    onGetAllProjectFailure = (err) => {
        const {history} = this.props;
        if(err) {
            history.push("/error")
        }
    }

    onProjectInputChange = (event) => {
        const result = projectNameSchema.validate({ project : event.target.value});
        if(result.error){
            this.setState({ error : true, errorMessage : result.error.details[0].message})
        } else {
            this.setState({ error : false, errorMessage : "" })
        }
        this.setState({
            newProjectInput : event.target.value
        })
        
    }

    onCreateNewProjectClick = () => {
        const { error, newProjectInput } = this.state;
        if(!error && newProjectInput.trim().length !== 0){
            const data = {name : newProjectInput.trim()}
            postProject(data, this.onPostProjectSuccess, this.onPostProjectFailure)
        } else {
            return false
        }
    }

    onPostProjectSuccess = (res) => {
        const {projects} = this.state;
        projects.push(res.data[0]);
        this.setState({projects : projects, newProjectInput : ""})
    }

    onPostProjectFailure = (err) => {
        const {history} = this.props;
        if(err) {
            history.push("/error")
        }
    }

    onProjectDeleteClick = (projectId) => {
        deleteSelectedProject(projectId, (res) => this.onProjectDeleteSuccess(res,projectId), this.onProjectDeleteFailure )
    }

    onProjectDeleteSuccess = (res, projectId) => {
        if(res.data.affectedRows !== 0){
            const newProjectState = this.state.projects.filter((project) => {
                return project.id !== projectId
            })
            this.setState({projects : newProjectState})
        }
    }

    onProjectDeleteFailure = (err) => {
        const {history} = this.props;
        if(err) {
            history.push("/error")
        }
    }

    onRedirectClickToIssue = (project) => {
        const { history } = this.props;
        const { projectName, projectId } = project;
        history.push({
            pathname : `/projects/${projectName}`,
            state : project
        })
    }

    render() {
        const {error, errorMessage, newProjectInput, projects} = this.state;
        return ( 
            <div className="project-screen-container">
                { error && <div className="error-container"><h6>{errorMessage}</h6></div>} 
                <Navbar 
                name="project" 
                placeholder="Enter Project Name" 
                buttonName="Create Project" 
                onInputChange={this.onProjectInputChange} 
                onCreateProjectClick={this.onCreateNewProjectClick} 
                updatedValue={newProjectInput}
                />
                <div className="project-dashboard">
                    <h2 id="dashboard-title">Project Dashboard</h2>
                    
                    <div className="dashboard-card-container">
                        {(projects && projects.length > 0) && projects.map((project) => {
                            const projectNameAndId= {projectName : project.name, projectId : project.id}
                            return <Card onClickRedirect={() => this.onRedirectClickToIssue(projectNameAndId)} key={project.id} onClick={() => this.onProjectDeleteClick(project.id)} projectName={project.name}/>
                        })}
                    </div>
                    
                </div>
            </div>
         );
    }
}

 
export default ProjectDashboard;