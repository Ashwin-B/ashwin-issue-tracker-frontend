const ErrorScreen = (props) => {
    return ( 
        <div className="error-outer-container">
            <h1>Internal Server Error.</h1>
            <p>Please try again later.</p>
        </div>
     );
}
 
export default ErrorScreen;