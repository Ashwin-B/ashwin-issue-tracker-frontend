import React, { Component } from 'react';
import Joi from "joi";
import CloseIcon from '@material-ui/icons/Close';
import InputField from "../components/inputField";
import Button from "../components/button";
import Navbar from "../components/navbar";
import IssueCard from "../components/issueCard";
import "./screens.css";
import { createNewIssue, getAllIssueWithProjectId, deleteIssue } from '../apiClient/apiRequest';

const inputNavStyle = {
    display : "none"
}

const issueFieldSchema = Joi.object({
    issueTitle : Joi.string().min(3).max(20).label("Issue Title"),
    issueContent : Joi.string().min(10).max(300).label("Issue Content")
})

class IssueScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            createIssueState : false,
            issueTitle : "",
            issueContent : "",
            issues : [],
            errors : {}
        }

    }

    componentDidMount() {
        const { projectId } = this.props.location.state;
        getAllIssueWithProjectId(projectId, this.onRetrieveIssueSuccess, this.onRetrieveIssueFailure)
    }

    onRetrieveIssueSuccess = (res) => {
        if(!res.data.error){
            this.setState({issues : res.data})
        }
    }

    onRetrieveIssueFailure = (err) => {
        const {history} = this.props;
        if(err){
            history.push("/error")
        }
    }

    onNewIssueClick = () => {
        this.setState({
            createIssueState : true
        })
    }

    onIssueInputChange = (event) => {
        const { errors } = this.state;
        const { name, value } = event.target;
        const issueObj  = {
            [name] : value,
        }
        const result = issueFieldSchema.validate(issueObj);
        if(result.error) {
            errors[name] = result.error.details[0].message
        } else {
            delete errors[name]
        }

        this.setState({
            [name] : value
        })
    }

    onCloseBtnClick = () => {
        this.setState({
            createIssueState : false,
        }) 
    }

    onCreateIssueClick = () => {
        const { projectId } = this.props.location.state;
        const { issueTitle, issueContent } = this.state;
        const issueData = {
            project_id : projectId,
            issue_title : issueTitle.trim(),
            issue : issueContent.trim(),
            status : "todo",
        }
        if(issueTitle.trim().length !== 0 && issueContent.trim().length !== 0){
            createNewIssue(issueData, this.onCreateIssueSuccess, this.onCreateIssueFailure)
        } else {
            return false
        }
    }

    onCreateIssueSuccess = (res) => {
        const {errors, issues} = this.state;
        if(Object.keys(errors).length === 0){
            issues.push(res.data[0])
            this.setState({ 
                issues : issues,
                createIssueState : false,
            })
        } else {
            return false
        }
    }

    onCreateIssueFailure = (err) => {
        const {history} = this.props;
        if(err){
            console.log(err)
            history.push("/error")
        }
    }

    onDeleteBtnClick = (issueId) => {
        deleteIssue(issueId, () => this.onDeleteIssueSuccess(issueId), this.onDeleteIssueFailure)
    }

    onDeleteIssueSuccess = (issueId) => {
        const {issues} = this.state;
        const updatedIssueState =  issues.filter((issue) => issue.id !== issueId )
        this.setState({
            issues : updatedIssueState
        })
    }

    onDeleteIssueFailure = (err) => {
        const {history} = this.props;
        if(err) {
            history.push("/error")
        }
    }

    onIssueCardClickRedirect = (issue) => {
        const { history } = this.props;
        const { projectName } = this.props.location.state;
        const { id } = issue;
        history.push({
            pathname : `/project/${projectName}/issues/${id}`,
            state : issue,
        })
    }

    render() { 
        const { issues, errors } = this.state;
        return ( 
            <React.Fragment>
                <Navbar onCreateProjectClick={this.onNewIssueClick} inputStyle={inputNavStyle} fieldname="issue" placeholder="Enter the issue" buttonName="NEW ISSUE"/>
                {!this.state.createIssueState ? 
                <div className="issue-outer-container">
                    <div className="issue-container">
                        <p className="issue-title">TO DO</p>
                        <div className="issue-screen-container">
                            { issues.length > 0 && issues.filter((issue) => issue.status === "todo").map((todoIssue) => <IssueCard key={todoIssue.id} issueTitle = {todoIssue.issue_title} issueContent={todoIssue.issue} onIssueCardClick= {() => this.onIssueCardClickRedirect(todoIssue)} onDeleteClick = {() => this.onDeleteBtnClick(todoIssue.id)}/>)}
                        </div>
                    </div>
                    <div className="issue-container">
                        <p className="issue-title">IN PROGRESS</p>
                        <div className="issue-screen-container">
                            { issues.length > 0 && issues.filter((issue) => issue.status === "inprogress").map((inProgressIssue) => <IssueCard key={inProgressIssue.id} issueTitle = {inProgressIssue.issue_title} issueContent={inProgressIssue.issue} onIssueCardClick= {() => this.onIssueCardClickRedirect(inProgressIssue)} onDeleteClick = {() => this.onDeleteBtnClick(inProgressIssue.id)}/>) }
                        </div>
                    </div>
                    <div className="issue-container">
                        <p className="issue-title">COMPLETED</p>
                        <div className="issue-screen-container">
                            { issues.length > 0 && issues.filter((issue) => issue.status === "completed").map((completedIssue) => <IssueCard key={completedIssue.id} issueTitle = {completedIssue.issue_title} issueContent={completedIssue.issue} onIssueCardClick= {() => this.onIssueCardClickRedirect(completedIssue)} onDeleteClick = {() => this.onDeleteBtnClick(completedIssue.id)}/>) }
                        </div>
                    </div>
                </div> :  
                <div className="create-issue-modal-container">
                    <div className="issue-close-btn">
                        <CloseIcon onClick={this.onCloseBtnClick}/>
                    </div>
                    <div className="issue-form-container">
                        <p>ISSUE TITLE</p>
                        <InputField name="issueTitle" style= {{border : "1px solid #663a82"}} placeholder="Enter Issue Title" onChange={this.onIssueInputChange}/>
                        {errors && <small className="error-message">{ errors.issueTitle }</small>}
                        <p>ISSUE CONTENT</p>
                        <textarea id="issue-content" name="issueContent" cols="40" rows="10" placeholder="Write your issue content down here" onChange={this.onIssueInputChange}/>
                        {errors && <small className="error-message">{ errors.issueContent }</small>}
                    </div>
                    <Button onClick={this.onCreateIssueClick} name="Create Issue"/>
                </div>
                }
            </React.Fragment>
         );
    }
}
 
export default IssueScreen;