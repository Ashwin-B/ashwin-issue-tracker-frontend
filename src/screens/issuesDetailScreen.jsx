import React, { Component } from 'react';
import { updateIssue, addComments, retrieveComments, deleteComments } from "../apiClient/apiRequest";
import Comments from "../components/comments";

class IssueDetailScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentIssue : this.props.location.state,
            comments : [],
            statusUpdated : this.props.location.state.status,
            commentsInput : "",
            commentUpdateState : false
        }
    }

    componentDidMount() {
        const { currentIssue } = this.state;
        retrieveComments(currentIssue.id, this.retrieveCommentsSuccess, this.retrieveCommentsFailure)
    }

    retrieveCommentsSuccess = (res) => {
        if(!res.data.error) {
            this.setState({
                comments : res.data
            })
        }
    }

    retrieveCommentsFailure = (err) => {
        if(err) {
            this.props.history.push("/error")
        }
    }

    handleStatusChange = (event) => {
        const { value } = event.target;
        console.log(value)
        this.setState({
            statusUpdated : value,
        })
    }

    handleClickSaveStatus = () => {
        const {currentIssue, statusUpdated} = this.state;
        const data = {
            project_id : currentIssue.project_id,
            issue_title : currentIssue.issue_title,
            issue : currentIssue.issue,
            status : statusUpdated
        }
        const issueId = currentIssue.id
        if(currentIssue.status !== statusUpdated) {
            updateIssue(issueId, data, () => this.updateIssueSuccess(statusUpdated), this.updateIssueFailure);
        } else {
            return false
        }
    }

    updateIssueSuccess = (status) => {
        const {currentIssue} = this.state;
        const {history} = this.props;

        currentIssue.status = status;
        this.setState({ currentIssue })

        history.goBack()

    }

    updateIssueFailure = (err) => {
        const {history} = this.props;
        if(err){
            history.push("/error")
        }
    }

    handleCommentsInput = (event) => {
        this.setState({
            commentsInput : event.target.value
        })
    }

    onClickAddComments = () => {
        const {currentIssue, commentsInput} = this.state
        const data = {
            issue_id : currentIssue.id,
            comment : commentsInput,
        }
        if(commentsInput.length !== 0){
            addComments(data, this.addCommentsSuccess, this.addCommentsFailure)
        } else {
            return false
        }
    }

    addCommentsSuccess = (res) => {
        this.state.comments.push(res.data[0])
        this.setState({
            comments : this.state.comments,
            commentsInput : "",
        })
    }

    addCommentsFailure = (err) => {
        console.log(err)
    }
    
    handleDeleteClick = (commentId) => {
        deleteComments(commentId,() => this.onDeleteSuccess(commentId), this.onDeleteFailure )
    }

    onDeleteSuccess = (commentId) => {
        const comments = this.state.comments.filter((comment) => comment.id !== commentId)
        this.setState({
            comments : comments
        })
    }

    onDeleteFailure = (err) => {
        const {history} = this.props;
        if(err){
            history.push("/error")
        }
    }

    render() { 
        const {issue_title, issue, status} = this.state.currentIssue;
        const {comments} = this.state
        return ( 
            <div className="issue-detail-outer-container">
                <div className="issue-detail-navbar">
                    <p>ISSUE TRACKER</p>
                </div>
                <div className="issue-detail-section">
                    <div className="project-details-container">
                        <h3>ISSUE TITLE</h3>
                        <p id="issue-detail-title">{issue_title}</p>
                        <h3>ISSUE CONTENT</h3>
                        <p id="issue-detail-content">{issue}</p>
                        <h3>CURRENT STATUS : <span id="issue-detail-status">{status.toUpperCase()}</span></h3>
                        <h3>UPDATE STATUS</h3>
                        <div className="status-dropdown"> 
                            <select name="issue-status" id="issue-status-box" onChange={this.handleStatusChange}>
                                <option value="todo">TO DO</option>
                                <option value="inprogress">IN PROGRESS</option>
                                <option value="completed">COMPLETED</option>
                            </select>
                            <button id="btn-update-status" onClick= {this.handleClickSaveStatus}>UPDATE STATUS</button>
                        </div>
                    </div>
                    <div className="comments-section-container">
                        <h3>COMMENTS</h3>
                        <input name="comments" id="comments-input" type="text" placeholder="Enter your Comment" onChange={this.handleCommentsInput}/>
                        <button id="btn-add-comments" onClick={this.onClickAddComments}>ADD COMMENTS</button>
                        <div className="comments-added-container">
                            {comments.length > 0 && comments.map((commentObj) => 
                            <Comments onDeleteClick={() => this.handleDeleteClick(commentObj.id)} key={commentObj.id} comment={commentObj.comment}/>
                            )}
                        </div>
                    </div>
                </div>
            </div>
         );
    }
}
 
export default IssueDetailScreen;